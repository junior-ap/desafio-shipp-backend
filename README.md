## Desafio Shipp Backend - Resolvido com Python e Django

### Instalar o pip
wget https://bootstrap.pypa.io/get-pip.py

sudo python3 get-pip.py

### Instalando virtualenv e instalar dependências 
A onde o projeto foi baixado 

sudo pip install virtualenv

virtualenv venv

. venv/bin/activate

pip install -r requirements.txt 

### Criando banco de dados

python install_migrations.py 

### Carregando os dados do stores.csv

python manage.py load_bd

### Rodando os testes do projeto

python manage.py test

### Executando o servidor local

python manage.py runserver

http://localhost:8000/V1/stores?latitude=42.746003&longitude=-73.691984

### Mais informações 
[Linkedin](https://www.linkedin.com/in/ant%C3%B4nio-junior-9a3b62140/)

[GitHub](https://github.com/antoniogsjunior)
