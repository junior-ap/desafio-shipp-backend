from django.urls import path, include

urlpatterns = [
    path('V1/stores', include('stores.api.urls', namespace='stores_api')),
]
