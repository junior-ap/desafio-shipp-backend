from geopy.distance import geodesic

from haversine import haversine

from stores.models import Stores
from stores.api.serializers import StoresFields, StoresSerializer


# Filtro das lojas por latitude e longitude
def stores_filter(latitude, longitude):
    distance_limit = 6.5
    init = (float(latitude), float(longitude))
    # Todas as lojas do banco de dados
    stores_all = Stores.objects.all()
    stores_data = []
    # For em todos os dados verificando se o mesmo está no raio da distância
    for store in stores_all:
        end = (store.latitude, store.longitude)
        distance = haversine(init, end)
        # Adiciona na lista apenas o que estão no limite de 6.5km
        if distance < distance_limit:
            stores_data.append(StoresFields(
                establishment_type=store.establishment_type, entity_name=store.entity_name,
                street_number=store.street_number, street_name=store.street_name, distance=round(distance, 2)))
    # Converte os dados em json
    serializer = StoresSerializer(stores_data, many=True)
    # Retorna os dados em ordem crescente com base na distancia
    return sorted(serializer.data, key=lambda k: k.get('distance', 0))
