from rest_framework import serializers


class StoresFields(object):
    def __init__(self, street_number, street_name, distance, entity_name, establishment_type):
        self.street_number = street_number
        self.street_name = street_name
        self.distance = distance
        self.entity_name = entity_name
        self.establishment_type = establishment_type


class StoresSerializer(serializers.Serializer):
    street_number = serializers.CharField(max_length=15)
    street_name = serializers.CharField(max_length=300)
    distance = serializers.CharField(max_length=20)
    entity_name = serializers.CharField(max_length=300)
    establishment_type = serializers.CharField(max_length=300)
