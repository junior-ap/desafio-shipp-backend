from django.urls import path

from . import viewsets

app_name = 'storesApi'
urlpatterns = [
    path('', viewsets.StoresListGenericView.as_view(), name='list'),
]
