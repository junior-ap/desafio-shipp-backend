from rest_framework import status, generics
from rest_framework.response import Response

from stores.api.filter import stores_filter


class StoresListGenericView(generics.RetrieveAPIView):

    def get(self, request, *args, **kwargs):
        return Response(stores_filter(request.GET.get('latitude'), request.GET.get('longitude')))
