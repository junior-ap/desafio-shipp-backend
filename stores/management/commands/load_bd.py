from django.core.management.base import BaseCommand
from stores.models import Stores

import pandas as pd
import json

from geopy.geocoders import GoogleV3
google_maps = GoogleV3(api_key='AIzaSyD2ikMvl3Xj1qRpz9YeSH_A7XeZSdy1j9s')


class Command(BaseCommand):
    def handle(self, *args, **kwargs):
        try:
            data = pd.read_csv("stores.csv")
            row = data.shape[0]
            i = 0
            while i < row:
                status_location = 1
                row_json = ''
                if data.values[i][14] == data.values[i][14]:
                    row_json = json.dumps(eval(data.values[i][14]))
                    row_json = json.loads(row_json)
                if 'longitude' not in row_json or 'latitude' not in row_json:
                    if data.values[i][12]:
                        result = google_maps.geocode(data.values[i][12])
                        if result:
                            longitude = result.longitude
                            latitude = result.latitude
                    else:
                        status_location = 0
                else:
                    longitude = row_json['longitude']
                    latitude = row_json['latitude']
                Stores.objects.create(street_number=data.values[i][6], street_name=data.values[i][7].strip(),
                                      status=status_location, longitude=longitude, latitude=latitude,
                                      city=data.values[i][10].strip(), establishment_type=data.values[i][3].strip(),
                                      entity_name=data.values[i][4].strip(), licenseNumber=data.values[i][1])
                i += 1
        except Exception as e:
            print(e)

