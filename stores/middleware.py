from django.http import JsonResponse
from django.utils.deprecation import MiddlewareMixin
from stores.models import LogSearches


# Middleware verificar se tem latitude e longitude no request
class RequestLogLat:
    def __init__(self, get_response):
        self.get_response = get_response

    def __call__(self, request):
        response = self.get_response(request)
        latitude = request.GET.get('latitude')
        longitude = request.GET.get('longitude')
        if hasattr(self, 'process_response'):
            response = self.process_response(request, response)
        if longitude and latitude:
            return response
        return JsonResponse(data={'error': 'Codenadas não encontradas'}, status=400)


# Middleware para guarda um log das pesquisar para relatorios
class ResponseMiddleware(MiddlewareMixin):

    def process_response(self, request, response):
        if response.status_code == 200:
            data = len(response.data)
        else:
            data = 0
        LogSearches.objects.create(latitude=request.GET.get('latitude'), longitude=request.GET.get('longitude'),
                                   status=response.status_code, amount_stores=data)
        return response
