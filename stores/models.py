from django.db import models


class Stores(models.Model):
    INCOMPLETE = 0
    COMPLETE = 1
    STATUS = (
        (INCOMPLETE, 'Incompleto'),
        (COMPLETE, 'Completo'),
    )

    status = models.IntegerField('Status', choices=STATUS, default=COMPLETE)
    licenseNumber = models.CharField('Número da Licença', max_length=50, null=True, blank=True)
    entity_name = models.CharField('Nome da Entidade', max_length=300, null=True, blank=True)
    establishment_type = models.CharField('Tipo de Estabelecimento', max_length=300, null=True, blank=True)
    street_number = models.CharField('Número da Rua', max_length=15, null=True, blank=True)
    street_name = models.CharField('Nome da Rua', max_length=300)
    city = models.CharField('Cidade', max_length=50)
    longitude = models.FloatField('Longitude', max_length=15, null=True, blank=True)
    latitude = models.FloatField('Latitude', max_length=15, null=True, blank=True)


class LogSearches(models.Model):
    status = models.IntegerField('Status')
    amount_stores = models.IntegerField('Qauntidade de Logas')
    longitude = models.CharField('Longitude', max_length=15, null=True, blank=True)
    latitude = models.CharField('Latitude', max_length=15, null=True, blank=True)
