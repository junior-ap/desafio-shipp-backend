from rest_framework import status
import unittest
import requests

end_point = 'http://localhost:8000/'


class Tests(unittest.TestCase):

    # Testa a api passando os parametros corretos
    def test_api_success(self):
        url = end_point + 'V1/stores?latitude=42.746003&longitude=-73.691984'
        response = requests.get(url)
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertEqual(response.json()[0]['street_number'], '165')
        self.assertEqual(response.json()[0]['street_name'], 'PAINE STREET')
        self.assertEqual(response.json()[0]['entity_name'], 'GREEN ISLAND MART INC')
        self.assertEqual(response.json()[0]['establishment_type'], 'A')

    # Testa a api faltando a latitude
    def test_api_fail_lon(self):
        url = end_point + 'V1/stores?longitude=-73.691984'
        response = requests.get(url)
        self.assertEqual(response.status_code, status.HTTP_400_BAD_REQUEST)

    # Testa a api faltando a longitude
    def test_api_fail_lat(self):
        url = end_point + 'V1/stores?latitude=42.746003'
        response = requests.get(url)
        self.assertEqual(response.status_code, status.HTTP_400_BAD_REQUEST)

    # Testa a api mandando json vazio
    def test_api_fail(self):
        url = end_point + 'V1/stores'
        response = requests.get(url)
        self.assertEqual(response.status_code, status.HTTP_400_BAD_REQUEST)
